﻿#ifndef _mat_op_def_h
#define _mat_op_def_h

#include "mat_def.h"

#define MAX(a, b)	((a > b) ? a : b)
#define MIN(a, b)	((a < b) ? a : b)

#define M_PI		(float)3.14159265358979323846
#define DEG_TO_RAD	(float)(M_PI/180.0)
#define RAD_TO_DEG	(float)(180.0/M_PI)

float nonZero(float x);
void matrixFree(matrix_instance_f32 *m);
void mat_init_f32(matrix_instance_f32 * S, uint16_t nRows, uint16_t nColumns, float * pData);
void fill_f32(float value, float * pDst, uint32_t blockSize);
float* matrixInit(matrix_instance_f32 *m, int rows, int cols);
float* MemInit(int size);
void memFree(float *d);
void mat_mult_f32( matrix_instance_f32 * pSrcA,  matrix_instance_f32 * pSrcB, matrix_instance_f32 * pDst);
void mat_trans_f32( matrix_instance_f32 * pSrc, matrix_instance_f32 * pDst);

int qrDecompositionT_f32(matrix_instance_f32 *A, matrix_instance_f32 *Q, matrix_instance_f32 *R);
void matrixDiv_f32(matrix_instance_f32 *X, matrix_instance_f32 *A, matrix_instance_f32 *B, matrix_instance_f32 *Q, matrix_instance_f32 *R, matrix_instance_f32 *AQ);

float dotVector3(float *va, float *vb);
void log_mapQuat(float *qIn, float *omega);
void exp_mapQuat(float *omega, float *q);
void box_plusQuat(float *qIn, float *vec, float *qOut);
void box_minusQuat(float *q1, float *q2, float *vec);
void normalizeVec3(float *vr, float *v);
void normalizeQuat(float *qr, float *q);
void crossVector3(float *vr, float *va, float *vb);
void quatMultiply(float *qr, float *q1, float *q2);
void rotateVectorByQuat(float *vr, float *v, float *q);
void rotateVectorByRevQuat(float *vr, float *v, float *q);
void rotateVecByMatrix(float *vr, float *v, float *m);
void rotateVecByRevMatrix(float *vr, float *v, float *m);
void quatToMatrix(float *m, float *q, int normalize);
void rotateQuat(float *qOut, float *qIn, float *rate);





















#endif