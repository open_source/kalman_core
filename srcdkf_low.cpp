﻿#include "srcdkf_low.h"
#include "math.h"
#include "mat_def.h"


extern "C"
{
	#include "mat_op.h"
}


srcdkf_low::srcdkf_low(int s, int m, int v, int n)
{
	int maxN = MAX(v, n);

	S = s;
	V = v;

	M = m;
	N = n;

	matrixInit(&Sx, s, s);
	matrixInit(&SxT, s, s);
	matrixInit(&Sv, v, v);
	matrixInit(&Sn, n, n);
	matrixInit(&x, s, 1);
	matrixInit(&Xa, s + maxN, 1 + (s + maxN) * 2);

	matrixInit(&qrTempS, s, (s + v) * 2);
	matrixInit(&y, m, 1);
	matrixInit(&Y, m, 1 + (s + n) * 2);
	matrixInit(&qrTempM, m, (s + n) * 2);
	matrixInit(&Sy, m, m);
	matrixInit(&SyT, m, m);
	matrixInit(&SyC, m, m);
	matrixInit(&Pxy, s, m);
	matrixInit(&C1, m, s);
	matrixInit(&C1T, s, m);
	matrixInit(&C2, m, n);
	matrixInit(&D, m, s + n);
	matrixInit(&K, s, m);
	matrixInit(&inov, m, 1);
	matrixInit(&xUpdate, s, 1);
	matrixInit(&qrFinal, s, 2 * s + 2 * n);

	matrixInit(&Q, s, s + n);	
	matrixInit(&R, n, n);	
	matrixInit(&AQ, s, n);	

	/************************/
	matrixInit(&Px, s, s);
	matrixInit(&Py, m, m);

	matrixInit(&KT, m, s);
	matrixInit(&inovT, 1, m);
	matrixInit(&rDiag, s, 1);
	/***********************/

	xOut = MemInit(s*sizeof(float));
	xNoise = MemInit(maxN* sizeof(float));
	xIn = MemInit(s* sizeof(float));


	h = SRCDKF_H;
	rm = SRCDKF_RM;
	hh = h*h;

	wim = 1.0f / (2.0f * hh);
	wic1 = sqrtf(1.0f / (4.0f * hh));
	wic2 = sqrtf((hh - 1.0f) / (4.0f * hh*hh));

}

float* srcdkf_low::GetState(void) {
	return x.pData;
}
void srcdkf_low::SetVariance(float *q, float *v, float *n, int nn) {
	float *Sxl = Sx.pData;
	float *Svl = Sv.pData;
	float *Snl = Sn.pData;
	int i;

	// state variance
	if (q)
		for (i = 0; i < S; i++)
			Sxl[i*S + i] = sqrtf(fabsf(q[i]));

	// process noise
	if (v)
		for (i = 0; i < V; i++)
			Svl[i*V + i] = sqrtf(fabsf(v[i]));

	// observation noise
	if (n && nn) {
		// resize Sn
		Sn.numRows = nn;
		Sn.numCols = nn;

		for (i = 0; i < nn; i++)
			Snl[i*nn + i] = sqrtf(fabsf(n[i]));
	}
}
void srcdkf_low::GetVariance(float *q) {
	float *Sxl = Sx.pData;
	int i;

	// state variance
	if (q)
		for (i = 0; i < S; i++) {
			q[i] = Sxl[i*S + i];
			q[i] = q[i] * q[i];
		}
}
void srcdkf_low::CalcSigmaPoints(matrix_instance_f32 *Sn) {

	int N = Sn->numRows;		// number of noise variables
	int A = S + N;			// number of agumented states
	int Ll = 1 + A * 2;			// number of sigma points
	float *xl = x.pData;	// state
	float *Sxl = Sx.pData;	// state covariance
	float *Xal = Xa.pData;	// augmented sigma points
	int i, j;

	// set the number of sigma points
	L = Ll;

	// resize output matrix
	Xa.numRows = A;
	Xa.numCols = L;

	//	-	   -
	// Sa =	| Sx	0  |
	//	| 0	Sn |
	//	-	   -
	// xa = [ x 	0  ]
	// Xa = [ xa  (xa + h*Sa)  (xa - h*Sa) ]
	//
	for (i = 0; i < A; i++) {
		int rOffset = i * L;
		float base = (i < S) ? xl[i] : 0.0f;

		Xal[rOffset + 0] = base;

		for (j = 1; j <= A; j++) {
			float t = 0.0f;

			if (i < S && j < S + 1)
				t = Sxl[i*S + (j - 1)] * h;

			if (i >= S && j >= S + 1)
				t = Sn->pData[(i - S)*N + (j - S - 1)] * h;

			Xal[rOffset + j] = base + t;
			Xal[rOffset + j + A] = base - t;
		}
	}
}

void srcdkf_low::TimeUpdate(float *u, float dt, SRCDKFTimeUpdate_t *timeUpdate) {
	
	int Ll;				// number of sigma points
	float *xl = x.pData;	// state estimate
	float *Xal = Xa.pData;	// augmented sigma points
	float *qrTempSl = qrTempS.pData;
	int i, j;

	CalcSigmaPoints(&Sv);
	Ll = L;

	timeUpdate(&Xal[0], &Xal[S*L], &Xal[0], u, dt, L);

	// sum weighted resultant sigma points to create estimated state
	w0m = (hh - (float)(S + V)) / hh;
	for (i = 0; i < S; i++) {
		int rOffset = i * L;

		xl[i] = Xal[rOffset + 0] * w0m;

		for (j = 1; j < L; j++)
			xl[i] += Xal[rOffset + j] * wim;
	}

	// update state covariance
	for (i = 0; i < S; i++) {
		int rOffset = i * (S + V) * 2;

		for (j = 0; j < S + V; j++) {
			qrTempSl[rOffset + j] = (Xal[i*L + j + 1] - Xal[i*L + S + V + j + 1]) * wic1;
			qrTempSl[rOffset + S + V + j] = (Xal[i*L + j + 1] + Xal[i*L + S + V + j + 1] - 2.0f*Xal[i*L + 0]) * wic2;
		}
	}

	qrDecompositionT_f32(&qrTempS, NULL, &SxT);   // with transposition
	mat_trans_f32(&SxT, &Sx);
}
void srcdkf_low::MeasurementUpdate(float *u, float *ym, int M, int N, float *noise, SRCDKFMeasurementUpdate_t *measurementUpdate) {
	float *Xal = Xa.pData;			// sigma points
	float *Yl = Y.pData;			// measurements from sigma points
	float *yl = y.pData;			// measurement estimate
	float *Snl = Sn.pData;			// observation noise covariance
	float *qrTempMl = qrTempM.pData;
	float *C1l = C1.pData;
	float *C1Tl = C1T.pData;
	float *C2l = C2.pData;
	float *Dl = D.pData;
	float *inovl = inov.pData;		// M x 1 matrix
	float *xUpdatel = xUpdate.pData;	// S x 1 matrix
	float *xl = x.pData;			// state estimate
	float *Sxl = Sx.pData;
	float *Ql = Q.pData;
	float *qrFinall = qrFinal.pData;
	int Ll;					// number of sigma points
	int i, j;

	// make measurement noise matrix if provided
	if (noise) {
		Sn.numRows = N;
		Sn.numCols = N;
		fill_f32(0.0f, Sn.pData, N*N);
		for (i = 0; i < N; i++)
			Snl[i*N + i] = sqrtf(fabsf(noise[i]));
	}

	// generate sigma points
	CalcSigmaPoints(&Sn);
	Ll = L;

	// resize all N and M based storage as they can change each iteration
	y.numRows = M;
	Y.numRows = M;
	Y.numCols = L;
	qrTempM.numRows = M;
	qrTempM.numCols = (S + N) * 2;
	Sy.numRows = M;
	Sy.numCols = M;
	SyT.numRows = M;
	SyT.numCols = M;
	SyC.numRows = M;
	SyC.numCols = M;
	Pxy.numCols = M;
	C1.numRows = M;
	C1T.numCols = M;
	C2.numRows = M;
	C2.numCols = N;
	D.numRows = M;
	D.numCols = S + N;
	K.numCols = M;
	inov.numRows = M;
	qrFinal.numCols = 2 * S + 2 * N;

	// Y = h(Xa, Xn)
	for (i = 0; i < L; i++) {
		for (j = 0; j < S; j++)
			xIn[j] = Xal[j*L + i];

		for (j = 0; j < N; j++)
			xNoise[j] = Xal[(S + j)*L + i];

		measurementUpdate(u, xIn, xNoise, xOut);

		for (j = 0; j < M; j++)
			Yl[j*L + i] = xOut[j];
	}

	// sum weighted resultant sigma points to create estimated measurement
	w0m = (hh - (float)(S + N)) / hh;
	for (i = 0; i < M; i++) {
		int rOffset = i * L;

		yl[i] = Yl[rOffset + 0] * w0m;

		for (j = 1; j < L; j++)
			yl[i] += Yl[rOffset + j] * wim;
	}

	// calculate measurement covariance components
	for (i = 0; i < M; i++) {
		int rOffset = i * (S + N) * 2;

		for (j = 0; j < S + N; j++) {
			float c, d;

			c = (Yl[i*L + j + 1] - Yl[i*L + S + N + j + 1]) * wic1;
			d = (Yl[i*L + j + 1] + Yl[i*L + S + N + j + 1] - 2.0f*Yl[i*L]) * wic2;

			qrTempMl[rOffset + j] = c;
			qrTempMl[rOffset + S + N + j] = d;

			// save fragments for future operations
			if (j < S) {
				C1l[i*S + j] = c;
				C1Tl[j*M + i] = c;
			}
			else {
				C2l[i*N + (j - S)] = c;
			}
			Dl[i*(S + N) + j] = d;
		}
	}

	qrDecompositionT_f32(&qrTempM, NULL, &SyT);	// with transposition

	mat_trans_f32(&SyT, &Sy);	
	//mat_trans_f32(&f->SyT, &f->SyC);		// make copy as later Div is destructive

	// create Pxy
	mat_mult_f32(&Sx, &C1T, &Pxy);

	// K = (Pxy / SyT) / Sy
	matrixDiv_f32(&K, &Pxy, &SyT, &Q, &R, &AQ);
	matrixDiv_f32(&K, &K, &Sy, &Q, &R, &AQ);

	// x = x + k(ym - y)
	for (i = 0; i < M; i++)
		inovl[i] = ym[i] - yl[i];
	mat_mult_f32(&K, &inov, &xUpdate);

	for (i = 0; i < S; i++)
		xl[i] += xUpdatel[i];

	// build final QR matrix
	//	rows = s
	//	cols = s + n + s + n
	//	use Q as temporary result storage

	Q.numRows = S;
	Q.numCols = S;
	mat_mult_f32(&K, &C1, &Q);
	for (i = 0; i < S; i++) {
		int rOffset = i * (2 * S + 2 * N);

		for (j = 0; j < S; j++)
			qrFinall[rOffset + j] = Sxl[i*S + j] - Ql[i*S + j];
	}

	Q.numRows = S;
	Q.numCols = N;
	mat_mult_f32(&K, &C2, &Q);
	for (i = 0; i < S; i++) {
		int rOffset = i * (2 * S + 2 * N);

		for (j = 0; j < N; j++)
			qrFinall[rOffset + S + j] = Ql[i*N + j];
	}

	Q.numRows = S;
	Q.numCols = S + N;
	mat_mult_f32(&K, &D, &Q);
	for (i = 0; i < S; i++) {
		int rOffset = i * (2 * S + 2 * N);

		for (j = 0; j < S + N; j++)
			qrFinall[rOffset + S + N + j] = Ql[i*(S + N) + j];
	}

	// Sx = qr([Sx-K*C1 K*C2 K*D]')
	// this method is not susceptable to numeric instability like the Cholesky is
	qrDecompositionT_f32(&qrFinal, NULL, &SxT);	// with transposition
	mat_trans_f32(&SxT, &Sx);
}

/**************************************/
void srcdkf_low::paramsrcdkfSetVariance(float *v, float *n) {
	float *rDiagl = rDiag.pData;
	int i;

	SetVariance( v, v, n, N);

	for (i = 0; i < S; i++)
		rDiagl[i] = 0.0;
}
void srcdkf_low::paramsrcdkfGetVariance(float *v, float *n) {
	float *Sxl = Sx.pData;
	float *Snl = Sn.pData;
	int i;

	// artificial parameter variance
	if (v)
		for (i = 0; i < S; i++) {
			v[i] = Sxl[i*S + i];
			v[i] = v[i] * v[i];
		}

	if (n)
		for (i = 0; i < N; i++) {
			n[i] = Snl[i*N + i];
			n[i] = n[i] * n[i];
		}
}
void srcdkf_low::paramsrcdkfSetRM(float rml) {
	rm = rml;
}
void srcdkf_low::paramsrcdkfUpdate(float *u, float *d, SRCDKFMeasurementUpdate_t *map) {

	float *Sxl = Sx.pData;
	float *Svl = Sv.pData;
	float *rDiagl = rDiag.pData;
	int i;

	MeasurementUpdate( u, d, M, N, 0, map);

	if (rm) {
		// Robbins-Monro innovation estimation for Rr
		// Rr = (1 - SRCDKF_RM)*Rr + SRCDKF_RM * K * [dk - g(xk, wk)] * [dk - g(xk, wk)]^T * K^T

		mat_trans_f32(&K, &KT);
		mat_trans_f32(&inov, &inovT);

		// xUpdate == K*inov
		mat_mult_f32(&xUpdate, &inovT, &K);
		mat_mult_f32(&K, &KT, &Sv);

		for (i = 0; i < S; i++) {
			rDiagl[i] = (1.0f - rm)*rDiagl[i] + rm * Svl[i*S + i];
			Sxl[i*S + i] = sqrtf(fabsf(Sxl[i*S + i] * Sxl[i*S + i] + rDiagl[i]));
		}
	}
}
/***********************************/

float *srcdkf_low::getCovarMatrix(void)
{
	mat_mult_f32(&Sx, &SxT, &Px);
	return Px.pData;
}
float *srcdkf_low::getSqrtCovarMatrix(void)
{
	return Sx.pData;
}

srcdkf_low::~srcdkf_low()
{
	matrixFree(&Sx);
	matrixFree(&SxT);
	matrixFree(&Sv);
	matrixFree(&Sn);
	matrixFree(&x);
	matrixFree(&Xa);

	matrixFree(&qrTempS);
	matrixFree(&y);
	matrixFree(&Y);
	matrixFree(&qrTempM);
	matrixFree(&Sy);
	matrixFree(&SyT);
	matrixFree(&SyC);
	matrixFree(&Pxy);
	matrixFree(&C1);
	matrixFree(&C1T);
	matrixFree(&C2);
	matrixFree(&D);
	matrixFree(&K);
	matrixFree(&inov);
	matrixFree(&xUpdate);
	matrixFree(&qrFinal);
	matrixFree(&Q);	// scratch
	matrixFree(&R);	// scratch
	matrixFree(&AQ);	// scratch

	/************************/
	matrixFree(&Px);
	matrixFree(&Py);
	matrixFree(&KT);
	matrixFree(&inovT);
	matrixFree(&rDiag);
	/***********************/
	memFree(xOut);
	memFree(xNoise);
	memFree(xIn);

}
