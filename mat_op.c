﻿#include "mat_op.h"
#include "mat_def.h"
#include "math.h"

#include "stdlib.h"


void mat_init_f32(	matrix_instance_f32 * S,	uint16_t nRows,	uint16_t nColumns,	float * pData)
{
	/* Assign Number of Rows */
	S->numRows = nRows;

	/* Assign Number of Columns */
	S->numCols = nColumns;

	/* Assign Data pointer */
	S->pData = pData;
}
void fill_f32(	float value,	float * pDst,	uint32_t blockSize)
{
	uint32_t blkCnt;                               /* loop counter */

												   /* Run the below code for Cortex-M4 and Cortex-M3 */
	float in1 = value;
	float in2 = value;
	float in3 = value;
	float in4 = value;

	/*loop Unrolling */
	blkCnt = blockSize >> 2u;

	/* First part of the processing with loop unrolling.  Compute 4 outputs at a time.
	** a second loop below computes the remaining 1 to 3 samples. */
	while (blkCnt > 0u)
	{
		/* C = value */
		/* Fill the value in the destination buffer */
		*pDst++ = in1;
		*pDst++ = in2;
		*pDst++ = in3;
		*pDst++ = in4;

		/* Decrement the loop counter */
		blkCnt--;
	}

	/* If the blockSize is not a multiple of 4, compute any remaining output samples here.
	** No loop unrolling is used. */
	blkCnt = blockSize % 0x4u;
	while (blkCnt > 0u)
	{
		/* C = value */
		/* Fill the value in the destination buffer */
		*pDst++ = value;

		/* Decrement the loop counter */
		blkCnt--;
	}
}
float* matrixInit(matrix_instance_f32 *m, int rows, int cols) {
	float *d;

	d = (float *)malloc(rows*cols*sizeof(float));

	mat_init_f32(m, rows, cols, d);
	fill_f32(0.0, d, rows*cols);
	return d;
}
float* MemInit(int size) {
	float *d;

	d = (float *)malloc(size);

	return d;
}
void matrixFree(matrix_instance_f32 *m)
{
	if (m&&m->pData)
	free((void*)m->pData);
}
void memFree(float *d)
{
	if (d)
		free((void*)d);
}

float nonZero(float x)
{

	//if ((x <  0.00000001f) && (x >  0.0f)) return 0.00000001f;
	//else if ((x <  0.0f) && (x > -0.00000001f)) return -0.00000001f;
	//else
	return x;
}
void mat_mult_f32(matrix_instance_f32 * pSrcA,	matrix_instance_f32 * pSrcB,	matrix_instance_f32 * pDst)
{
	float *pIn1 = pSrcA->pData;                /* input data matrix pointer A */
	float *pIn2 = pSrcB->pData;                /* input data matrix pointer B */
	float *pInA = pSrcA->pData;                /* input data matrix pointer A  */
	float *pOut = pDst->pData;                 /* output data matrix pointer */
	float *px;                                 /* Temporary output data matrix pointer */
	float sum;                                 /* Accumulator */
	uint16_t numRowsA = pSrcA->numRows;            /* number of rows of input matrix A */
	uint16_t numColsB = pSrcB->numCols;            /* number of columns of input matrix B */
	uint16_t numColsA = pSrcA->numCols;            /* number of columns of input matrix A */


												   /* Run the below code for Cortex-M4 and Cortex-M3 */

	float in1, in2, in3, in4;
	uint32_t col, i = 0u, j, row = numRowsA, colCnt;      /* loop counters */


	{
		/* The following loop performs the dot-product of each row in pSrcA with each column in pSrcB */
		/* row loop */
		do
		{
			/* Output pointer is set to starting address of the row being processed */
			px = pOut + i;

			/* For every row wise process, the column loop counter is to be initiated */
			col = numColsB;

			/* For every row wise process, the pIn2 pointer is set
			** to the starting address of the pSrcB data */
			pIn2 = pSrcB->pData;

			j = 0u;

			/* column loop */
			do
			{
				/* Set the variable sum, that acts as accumulator, to zero */
				sum = 0.0f;

				/* Initiate the pointer pIn1 to point to the starting address of the column being processed */
				pIn1 = pInA;

				/* Apply loop unrolling and compute 4 MACs simultaneously. */
				colCnt = numColsA >> 2u;

				/* matrix multiplication        */
				while (colCnt > 0u)
				{
					/* c(m,n) = a(1,1)*b(1,1) + a(1,2) * b(2,1) + .... + a(m,p)*b(p,n) */
					in3 = *pIn2;
					pIn2 += numColsB;
					in1 = pIn1[0];
					in2 = pIn1[1];
					sum += in1 * in3;
					in4 = *pIn2;
					pIn2 += numColsB;
					sum += in2 * in4;

					in3 = *pIn2;
					pIn2 += numColsB;
					in1 = pIn1[2];
					in2 = pIn1[3];
					sum += in1 * in3;
					in4 = *pIn2;
					pIn2 += numColsB;
					sum += in2 * in4;
					pIn1 += 4u;

					/* Decrement the loop count */
					colCnt--;
				}

				/* If the columns of pSrcA is not a multiple of 4, compute any remaining MACs here.
				** No loop unrolling is used. */
				colCnt = numColsA % 0x4u;

				while (colCnt > 0u)
				{
					/* c(m,n) = a(1,1)*b(1,1) + a(1,2) * b(2,1) + .... + a(m,p)*b(p,n) */
					sum += *pIn1++ * (*pIn2);
					pIn2 += numColsB;

					/* Decrement the loop counter */
					colCnt--;
				}

				/* Store the result in the destination buffer */
				*px++ = sum;

				/* Update the pointer pIn2 to point to the  starting address of the next column */
				j++;
				pIn2 = pSrcB->pData + j;

				/* Decrement the column loop counter */
				col--;

			} while (col > 0u);



			/* Update the pointer pInA to point to the  starting address of the next row */
			i = i + numColsB;
			pInA = pInA + numColsA;

			/* Decrement the row loop counter */
			row--;

		} while (row > 0u);
	}


}
void mat_trans_f32(	matrix_instance_f32 * pSrc,	matrix_instance_f32 * pDst)
{
	float *pIn = pSrc->pData;                  /* input data matrix pointer */
	float *pOut = pDst->pData;                 /* output data matrix pointer */
	float *px;                                 /* Temporary output data matrix pointer */
	uint16_t nRows = pSrc->numRows;                /* number of rows */
	uint16_t nColumns = pSrc->numCols;             /* number of columns */
												   /* Run the below code for Cortex-M4 and Cortex-M3 */

	uint32_t blkCnt, i = 0u, row = nRows;          /* loop counters */


	{
		/* Matrix transpose by exchanging the rows with columns */
		/* row loop     */
		do
		{
			/* Loop Unrolling */
			blkCnt = nColumns >> 2;

			/* The pointer px is set to starting address of the column being processed */
			px = pOut + i;

			/* First part of the processing with loop unrolling.  Compute 4 outputs at a time.
			** a second loop below computes the remaining 1 to 3 samples. */
			while (blkCnt > 0u)        /* column loop */
			{
				/* Read and store the input element in the destination */
				*px = *pIn++;

				/* Update the pointer px to point to the next row of the transposed matrix */
				px += nRows;

				/* Read and store the input element in the destination */
				*px = *pIn++;

				/* Update the pointer px to point to the next row of the transposed matrix */
				px += nRows;

				/* Read and store the input element in the destination */
				*px = *pIn++;

				/* Update the pointer px to point to the next row of the transposed matrix */
				px += nRows;

				/* Read and store the input element in the destination */
				*px = *pIn++;

				/* Update the pointer px to point to the next row of the transposed matrix */
				px += nRows;

				/* Decrement the column loop counter */
				blkCnt--;
			}

			/* Perform matrix transpose for last 3 samples here. */
			blkCnt = nColumns % 0x4u;

			while (blkCnt > 0u)
			{
				/* Read and store the input element in the destination */
				*px = *pIn++;

				/* Update the pointer px to point to the next row of the transposed matrix */
				px += nRows;

				/* Decrement the column loop counter */
				blkCnt--;
			}



			i++;

			/* Decrement the row loop counter */
			row--;

		} while (row > 0u);          /* row loop end  */

	}


}
int qrDecompositionT_f32(matrix_instance_f32 *A, matrix_instance_f32 *Q, matrix_instance_f32 *R) {
	int minor;
	int row, col;
	int m = A->numCols;
	int n = A->numRows;
	int min;

	// clear R
	fill_f32(0, R->pData, R->numRows*R->numCols);

	min = MIN(m, n);

	/*
	* The QR decomposition of a matrix A is calculated using Householder
	* reflectors by repeating the following operations to each minor
	* A(minor,minor) of A:
	*/
	for (minor = 0; minor < min; minor++) {
		float xNormSqr = 0.0f;
		float a;

		/*
		* Let x be the first column of the minor, and a^2 = |x|^2.
		* x will be in the positions A[minor][minor] through A[m][minor].
		* The first column of the transformed minor will be (a,0,0,..)'
		* The sign of a is chosen to be opposite to the sign of the first
		* component of x. Let's find a:
		*/
		for (row = minor; row < m; row++)
			xNormSqr += A->pData[minor*m + row] * A->pData[minor*m + row];

		a = sqrtf(fabsf(xNormSqr));
		if (A->pData[minor*m + minor] > 0.0f)
			a = -a;

		if (a != 0.0f) {
			R->pData[minor*R->numCols + minor] = a;

			/*
			* Calculate the normalized reflection vector v and transform
			* the first column. We know the norm of v beforehand: v = x-ae
			* so |v|^2 = <x-ae,x-ae> = <x,x>-2a<x,e>+a^2<e,e> =
			* a^2+a^2-2a<x,e> = 2a*(a - <x,e>).
			* Here <x, e> is now A[minor][minor].
			* v = x-ae is stored in the column at A:
			*/
			A->pData[minor*m + minor] -= a; // now |v|^2 = -2a*(A[minor][minor])

											/*
											* Transform the rest of the columns of the minor:
											* They will be transformed by the matrix H = I-2vv'/|v|^2.
											* If x is a column vector of the minor, then
											* Hx = (I-2vv'/|v|^2)x = x-2vv'x/|v|^2 = x - 2<x,v>/|v|^2 v.
											* Therefore the transformation is easily calculated by
											* subtracting the column vector (2<x,v>/|v|^2)v from x.
											*
											* Let 2<x,v>/|v|^2 = alpha. From above we have
											* |v|^2 = -2a*(A[minor][minor]), so
											* alpha = -<x,v>/(a*A[minor][minor])
											*/
			for (col = minor + 1; col < n; col++) {
				float alpha = 0.0f;

				for (row = minor; row < m; row++)
					alpha -= A->pData[col*m + row] * A->pData[minor*m + row];

				alpha /= nonZero(a*A->pData[minor*m + minor]);

				// Subtract the column vector alpha*v from x.
				for (row = minor; row < m; row++)
					A->pData[col*m + row] -= alpha*A->pData[minor*m + row];
			}
		}
		// rank deficient
		else
			return 0;
	}

	// Form the matrix R of the QR-decomposition.
	//      R is supposed to be m x n, but only calculate n x n
	// copy the upper triangle of A
	for (row = min - 1; row >= 0; row--)
		for (col = row + 1; col < n; col++)
			R->pData[row*R->numCols + col] = A->pData[col*m + row];

	// Form the matrix Q of the QR-decomposition.
	//      Q is supposed to be m x m

	// only compute Q if requested
	if (Q) {
		fill_f32(0, Q->pData, Q->numRows*Q->numCols);

		/*
		* Q = Q1 Q2 ... Q_m, so Q is formed by first constructing Q_m and then
		* applying the Householder transformations Q_(m-1),Q_(m-2),...,Q1 in
		* succession to the result
		*/
		for (minor = m - 1; minor >= min; minor--)
			Q->pData[minor*m + minor] = 1.0f;

		for (minor = min - 1; minor >= 0; minor--) {
			Q->pData[minor * m + minor] = 1.0f;

			if (A->pData[minor*m + minor] != 0.0f) {
				for (col = minor; col < m; col++) {
					float alpha = 0.0f;

					for (row = minor; row < m; row++)
						alpha -= Q->pData[row*m + col] * A->pData[minor*m + row];

					alpha /= nonZero(R->pData[minor*R->numCols + minor] * A->pData[minor*m + minor]);

					for (row = minor; row < m; row++)
						Q->pData[row*m + col] -= alpha*A->pData[minor*m + row];
				}
			}
		}
	}
	return 1;
}
void matrixDiv_f32(matrix_instance_f32 *X, matrix_instance_f32 *A, matrix_instance_f32 *B, matrix_instance_f32 *Q, matrix_instance_f32 *R, matrix_instance_f32 *AQ) {
	int i, j, k;
	int m, n;

	// this is messy (going into a class's private data structure),
	// but it is better than malloc/free
	Q->numRows = B->numRows;
	Q->numCols = B->numRows;
	R->numRows = B->numRows;
	R->numCols = B->numCols;
	AQ->numRows = A->numRows;
	AQ->numCols = B->numRows;

	m = A->numRows;
	n = B->numCols;

	qrDecompositionT_f32(B, Q, R);
	mat_mult_f32(A, Q, AQ);

	// solve for X by backsubstitution
	for (i = 0; i < m; i++) {
		for (j = n - 1; j >= 0; j--) {
			for (k = j + 1; k < n; k++)
				AQ->pData[i*n + j] -= R->pData[j*n + k] * X->pData[i*n + k];
			X->pData[i*n + j] = AQ->pData[i*n + j] / nonZero(R->pData[j*n + j]);
		}
	}
}

float dotVector3(float *va, float *vb) {
	return va[0] * vb[0] + va[1] * vb[1] + va[2] * vb[2];
}
void log_mapQuat(float *qIn, float *omega) {

	// define these compile time constants to avoid std::abs:
	static const float twoPi = 2.0f * M_PI, NearlyOne = 1.0f - 1e-8f,
		NearlyNegativeOne = -1.0f + 1e-8f;

	const float qw = qIn[0];
	// See Quaternion-Logmap.nb in doc for Taylor expansions
	if (qw >= NearlyOne) {
		// Taylor expansion of (angle / s) at 1
		// (2 + 2 * (1-qw) / 3) * q.vec();
		float tmp = (8.0f / 3.0f - 2.0f / 3.0f * qw);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}
	else if (qw <= NearlyNegativeOne) {
		// Taylor expansion of (angle / s) at -1
		// (-2 - 2 * (1 + qw) / 3) * q.vec();
		float tmp = (-8.0f / 3.0f - 2.0f / 3.0f * qw);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}
	else {
		// Normal, away from zero case
		float angle = 2.0f * acosf(qw), s = sqrtf(1.0f - qw * qw);
		// Important:  convert to [-pi,pi] to keep error continuous
		if (angle > M_PI)
			angle -= twoPi;
		else if (angle < -M_PI)
			angle += twoPi;
		float tmp = (angle / s);
		omega[0] = tmp * qIn[1];
		omega[1] = tmp * qIn[2];
		omega[2] = tmp * qIn[3];
	}

}
void exp_mapQuat(float *omega, float *q) {
	float vec[3];
	float theta2 = dotVector3(omega, omega);
	if (theta2 > 5.96e-8f) {
		float theta = sqrtf(theta2);
		float ha = 0.5f * theta;
		float tmp = (sinf(ha) / theta);

		vec[0] = tmp * omega[0];
		vec[1] = tmp * omega[1];
		vec[2] = tmp * omega[2];
		q[0] = cosf(ha);
		q[1] = vec[0];
		q[2] = vec[1];
		q[3] = vec[2];
	}
	else {
		// first order approximation sin(theta/2)/theta = 0.5
		vec[0] = 0.5f * omega[0];
		vec[1] = 0.5f * omega[1];
		vec[2] = 0.5f * omega[2];
		q[0] = 1.0f;
		q[1] = vec[0];
		q[2] = vec[1];
		q[3] = vec[2];
	}
}
void box_plusQuat(float *qIn, float *vec, float *qOut) {
	float qexp[4];

	if ((vec[0] == 0.0f) && (vec[1] == 0.0f) && (vec[2] == 0.0f))
	{
		qOut[0] = qIn[0];
		qOut[1] = qIn[1];
		qOut[2] = qIn[2];
		qOut[3] = qIn[3];
		return;
	}
	exp_mapQuat(vec, qexp);
	quatMultiply(qOut, qexp, qIn);
}

void box_minusQuat(float *q1, float *q2, float *vec) {
	float q2inv[4], qr[4];
	if ((q1[0] == q2[0]) && (q1[1] == q2[1]) && (q1[2] == q2[2]) && (q1[3] == q2[3]))
	{
		vec[0] = 0.0f;
		vec[1] = 0.0f;
		vec[2] = 0.0f;
		return;
	}
	q2inv[0] = q2[0];
	q2inv[1] = -q2[1];
	q2inv[2] = -q2[2];
	q2inv[3] = -q2[3];
	quatMultiply(qr, q1, q2inv);
	log_mapQuat(qr, vec);
}
void quatMultiply(float *qr, float *q1, float *q2) {
	qr[0] = q2[0] * q1[0] - q2[1] * q1[1] - q2[2] * q1[2] - q2[3] * q1[3];
	qr[1] = q2[0] * q1[1] + q2[1] * q1[0] - q2[2] * q1[3] + q2[3] * q1[2];
	qr[2] = q2[0] * q1[2] + q2[1] * q1[3] + q2[2] * q1[0] - q2[3] * q1[1];
	qr[3] = q2[0] * q1[3] - q2[1] * q1[2] + q2[2] * q1[1] + q2[3] * q1[0];
}
void normalizeVec3(float *vr, float *v) {
	float norm;

	norm = sqrtf(v[0] * v[0] + v[1] * v[1] + v[2] * v[2]);

	vr[0] = v[0] / norm;
	vr[1] = v[1] / norm;
	vr[2] = v[2] / norm;
}
void normalizeQuat(float *qr, float *q) {
	float norm;

	norm = 1.0f / sqrtf(q[0] * q[0] + q[1] * q[1] + q[2] * q[2] + q[3] * q[3]);

	qr[0] *= norm;
	qr[1] *= norm;
	qr[2] *= norm;
	qr[3] *= norm;
}
void crossVector3(float *vr, float *va, float *vb) {
	vr[0] = va[1] * vb[2] - vb[1] * va[2];
	vr[1] = va[2] * vb[0] - vb[2] * va[0];
	vr[2] = va[0] * vb[1] - vb[0] * va[1];
}
void rotateVectorByQuat(float *vr, float *v, float *q) {
	float w, x, y, z;

	w = q[0];
	x = q[1];
	y = q[2];
	z = q[3];

	vr[0] = w * w*v[0] + 2.0f*y*w*v[2] - 2.0f*z*w*v[1] + x * x*v[0] + 2.0f*y*x*v[1] + 2.0f*z*x*v[2] - z * z*v[0] - y * y*v[0];
	vr[1] = 2.0f*x*y*v[0] + y * y*v[1] + 2.0f*z*y*v[2] + 2.0f*w*z*v[0] - z * z*v[1] + w * w*v[1] - 2.0f*x*w*v[2] - x * x*v[1];
	vr[2] = 2.0f*x*z*v[0] + 2.0f*y*z*v[1] + z * z*v[2] - 2.0f*w*y*v[0] - y * y*v[2] + 2.0f*w*x*v[1] - x * x*v[2] + w * w*v[2];
}
void rotateVectorByRevQuat(float *vr, float *v, float *q) {
	float qc[4];

	qc[0] = q[0];
	qc[1] = -q[1];
	qc[2] = -q[2];
	qc[3] = -q[3];

	rotateVectorByQuat(vr, v, qc);
}
void rotateVecByMatrix(float *vr, float *v, float *m) {
	vr[0] = m[0 * 3 + 0] * v[0] + m[0 * 3 + 1] * v[1] + m[0 * 3 + 2] * v[2];
	vr[1] = m[1 * 3 + 0] * v[0] + m[1 * 3 + 1] * v[1] + m[1 * 3 + 2] * v[2];
	vr[2] = m[2 * 3 + 0] * v[0] + m[2 * 3 + 1] * v[1] + m[2 * 3 + 2] * v[2];
}
void rotateVecByRevMatrix(float *vr, float *v, float *m) {
	vr[0] = m[0 * 3 + 0] * v[0] + m[1 * 3 + 0] * v[1] + m[2 * 3 + 0] * v[2];
	vr[1] = m[0 * 3 + 1] * v[0] + m[1 * 3 + 1] * v[1] + m[2 * 3 + 1] * v[2];
	vr[2] = m[0 * 3 + 2] * v[0] + m[1 * 3 + 2] * v[1] + m[2 * 3 + 2] * v[2];
}
void quatToMatrix(float *m, float *q, int normalize) {
	float sqw = q[0] * q[0];
	float sqx = q[1] * q[1];
	float sqy = q[2] * q[2];
	float sqz = q[3] * q[3];
	float tmp1, tmp2;
	float invs;

	// get the invert square length
	if (normalize)
		invs = 1.0f / (sqx + sqy + sqz + sqw);
	else
		invs = 1.0f;

	// rotation matrix is scaled by inverse square length
	m[0 * 3 + 0] = (sqx - sqy - sqz + sqw) * invs;
	m[1 * 3 + 1] = (-sqx + sqy - sqz + sqw) * invs;
	m[2 * 3 + 2] = (-sqx - sqy + sqz + sqw) * invs;

	tmp1 = q[1] * q[2];
	tmp2 = q[3] * q[0];
	m[1 * 3 + 0] = 2.0f * (tmp1 + tmp2) * invs;
	m[0 * 3 + 1] = 2.0f * (tmp1 - tmp2) * invs;

	tmp1 = q[1] * q[3];
	tmp2 = q[2] * q[0];
	m[2 * 3 + 0] = 2.0f * (tmp1 - tmp2) * invs;
	m[0 * 3 + 2] = 2.0f * (tmp1 + tmp2) * invs;

	tmp1 = q[2] * q[3];
	tmp2 = q[1] * q[0];
	m[2 * 3 + 1] = 2.0f * (tmp1 + tmp2) * invs;
	m[1 * 3 + 2] = 2.0f * (tmp1 - tmp2) * invs;
}

void rotateQuat(float *qOut, float *qIn, float *rate) {
	float q[4];
	float r[3];

	r[0] = rate[0] * -0.5f;
	r[1] = rate[1] * -0.5f;
	r[2] = rate[2] * -0.5f;

	q[0] = qIn[0];
	q[1] = qIn[1];
	q[2] = qIn[2];
	q[3] = qIn[3];

	// rotate
	qOut[0] = q[0] + r[0] * q[1] + r[1] * q[2] + r[2] * q[3];
	qOut[1] = -r[0] * q[0] + q[1] - r[2] * q[2] + r[1] * q[3];
	qOut[2] = -r[1] * q[0] + r[2] * q[1] + q[2] - r[0] * q[3];
	qOut[3] = -r[2] * q[0] - r[1] * q[1] + r[0] * q[2] + q[3];
}















