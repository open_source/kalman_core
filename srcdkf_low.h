﻿#ifndef _srcdkf_low_h
#define _srcdkf_low_h

#include "mat_def.h"

#define SRCDKF_H	(sqrtf(3.0f) * 3.0f)
#define SRCDKF_RM	0.0001f		// Robbins-Monro stochastic term

typedef void SRCDKFTimeUpdate_t(float *x_I, float *noise_I, float *x_O, float *u, float dt, int n);
typedef void SRCDKFMeasurementUpdate_t(float *u, float *x, float *noise_I, float *y);

class srcdkf_low
{
private:
	int S;
	int V;
	int M;		// only used for parameter estimation
	int N;		// only used for parameter estimation
	int L;

	float h;
	float hh;
	float w0m, wim, wic1, wic2;
	float rm;
	float mahTh;

	matrix_instance_f32 Sx;	// state covariance
	matrix_instance_f32 SxT;	// Sx transposed
	matrix_instance_f32 Sv;	// process noise
	matrix_instance_f32 Sn;	// observation noise
	matrix_instance_f32 x;	// state estimate vector
	matrix_instance_f32 Xa;	// augmented sigma points
	float *xIn;
	float *xNoise;
	float *xOut;
	matrix_instance_f32 qrTempS;
	matrix_instance_f32 Y;	// resultant measurements from sigma points
	matrix_instance_f32 y;	// measurement estimate vector
	matrix_instance_f32 qrTempM;
	matrix_instance_f32 Sy;	// measurement covariance
	matrix_instance_f32 SyT;	// Sy transposed
	matrix_instance_f32 SyC;	// copy of Sy
	matrix_instance_f32 Pxy;
	matrix_instance_f32 C1;
	matrix_instance_f32 C1T;
	matrix_instance_f32 C2;
	matrix_instance_f32 D;
	matrix_instance_f32 K;
	matrix_instance_f32 KT;	// only used for param est
	matrix_instance_f32 inov;	// inovation
	matrix_instance_f32 inovT;// only used for param est
	matrix_instance_f32 xUpdate;
	matrix_instance_f32 qrFinal;
	matrix_instance_f32 rDiag;
	matrix_instance_f32 Q, R, AQ;	// scratch

	matrix_instance_f32 Px;	// state  true covariance
	matrix_instance_f32 Py;	// measurment  true covariance

	SRCDKFMeasurementUpdate_t *map;	// only used for param est


	void CalcSigmaPoints(matrix_instance_f32 *Sn);

public:

	float* GetState(void);
	float *getCovarMatrix(void);
	float *getSqrtCovarMatrix(void);
	void SetVariance(float *q, float *v, float *n, int nn);
	void GetVariance(float *q);

	void TimeUpdate(float *u, float dt, SRCDKFTimeUpdate_t *timeUpdate);
	void MeasurementUpdate(float *u, float *ym, int M, int N, float *noise, SRCDKFMeasurementUpdate_t *measurementUpdate);

	/********************/
	void paramsrcdkfSetVariance(float *v, float *n);
	void paramsrcdkfGetVariance(float *v, float *n);
	void paramsrcdkfSetRM(float rml);
	void paramsrcdkfUpdate(float *u, float *d, SRCDKFMeasurementUpdate_t *map);
	/**************************/

	srcdkf_low(int s, int m, int v, int n);
	~srcdkf_low();
};

#endif